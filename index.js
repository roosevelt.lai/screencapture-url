/**
 * Copyright (c) 2018 Alexander Lang <alexander.m.lang@googlemail.com>
 *
 * Licensed under the MIT License
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://choosealicense.com/licenses/mit
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * @author Alexander Lang <alexander.m.lang@googlemail.com>
 * @copyright 2018 Alexander Lang <alexander.m.lang@googlemail.com>
 * @license MIT https://choosealicense.com/licenses/mit
 */
'use strict';

const assert = require('assert');
const os = require('os');
const path = require('path');
const fs = require('fs');
const {Url} = require('url');
const uuidv4 = require('uuid/v4');
const puppeteer = require('puppeteer');

class SystemError {
  constructor(message, code, errno) {
    const err = new Error(message);
    err.code = code;
    err.errno = errno;
    return err;
  }
}
/**
 * @example
 * ```
 * // fast example
 * (async () =>
 *    const result = await new Screenshot()
 *        .of('http://example.com')
 *        .to('.')
 *        .withName('my_screenshot')
 *        .asPng()
 *        .take()
 *    console.log(result) // Output: { path: './my_screenshot.png', screenshot: <Buffer 89 50 4e 47 0d 0a ...>  }
 * )()
 * ```
 */
class ScreenCapture {
/**
 * ScreenCapture()
   * @param {String|Url} [url='null'] Url to capture
   * @param {Object} [options={}] Options how to screenshot URL
   * @example
   * ```
   *  const { ScreenCapture } = require('screencapture-url')
   *  ...
   *  new ScreenCapture()
   *  // or
   *  new ScreenCapture('http://example.com')
   *  // or
   *  new ScreenCapture('http://example.com', {
   *     //  clipping path with x and y position and screencapture area
   *     // alternativly clip as array `clip: [0,0, 1024, 768],
   *     clip: {
   *       x: 0,
   *       y: 0,
   *       width: 1024,
   *       height: 768
   *     },
   *     // output format jpeg or png
   *     type: 'png',
   *     // output encoding
   *     encoding: 'binary',
   *     // pupetteer launch options
   *     launch: {}
   *     // path where the image should be saved default os.tmpdir()
   *     path: '/var/folders/07/gf60ws1n7pz1jdyjgfpcw8200xxrr3/T',
   *     // name of the image, without extension, extension is appended, e.g. asd => asd.png => asd.png.png => asd.png.png.png and so forth ...
   *     name: '9b9e2695-be33-4358-ae97-c922d34b4b77'
   *  })
   * ```
   */
  constructor(url = null, options = {}) {
    if (url) {
      this.of(url);
    } else {
      this.url = null;
    }
    this.saveAsText = false;
    this.options = {clip: {}};
    this.clip(options.clip || {x: 0, y: 0, width: 1024, height: 768})
      .as(options.type || 'png')
      .as(options.encoding || 'binary')
      .to(options.path)
      .withName(options.name);
    this.options.launch = options.launch || {};
  }

  /**
   * Alias for `of()`
   * @example
   * ```
   *  new ScreenCapture().from('http://example.com')
   * ```
   * @param {String|Url} uri URI to screencapture
   * @see of()
   * @returns {this} chainable
   */
  from(uri) {
    return this.of(uri);
  }

  /**
   * Sets the url to screencapture
   * @example
   * ```
   *  new ScreenCapture().of('http://example.com')
   * ```
   * @param {String|Url} uri URI to screencapture
   * @returns {this} chainable
   */
  of(uri) {
    assert(uri instanceof Url || typeof uri === 'string');
    this.url = uri instanceof Url ? uri.href : uri;
    return this;
  }

  /**
   * Sets the directory or file path to where the image should be saved
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('/path/to/save') // or .to('.')
   *   // or
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    // sets path and screenshot identifier
   *    .to('/path/to/screenshot.png')
   * ```
   * @param {String} [savepath='null'] Path (only the directory or directory with filename) where the screenshot should be saved.
   * @throws {SystemError} ENOENT if file or directory could not be found
   * @returns {this} chainable
   */
  to(savepath = null) {
    const isTempDir = savepath === null;
    if (isTempDir) {
      savepath = os.tmpdir();
    }
    assert(fs.existsSync(path.dirname(savepath)), new SystemError(`ENOENT: No such file or directory, '${savepath}'`, 'ENOENT', 2));
    this.options.path = path.resolve(path.dirname(savepath));
    const posibleName = path.basename(savepath);
    if (!isTempDir) {
      this.withName(posibleName.replace(/^\.+?/, ''));
    }

    return this;
  }

  /**
   * Sets the identifier of the image
   * - default is an uuidv4 String
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .withName('my_screenshot.png')
   * ```
   * @param {String} [name='null'] Screenshot filename, if name is null an uuid v4 string would set as name
   * @see {@link to}
   * @returns {this} chainable
   */
  withName(name = null) {
    this.options.name = name || uuidv4();
    return this;
  }

  /**
   * Sets the height and width of the clipping path
   * - shortcut for `.width(..).height(..)`
   * - default is 1024px to 768px @see constructor
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .withName('my_screenshot.png')
   *    .withDimensions(100, 100) // width and height in pixels
   * ```
   * @param {Number} width screenshot width
   * @param {Number} height screenshot height
   * @see width(), height()
   * @returns {this} chainable
   */
  withDimensions(width, height) {
    return this.width(width).height(height);
  }

  /**
   * Sets the width of the clipping path
   * - default is 1024px @see constructor
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .width(100) // in pixels
   * ```
   * @param {Number} width screenshot width
   * @see withDimensions(), height()
   * @throws {AssertionError} if width <= 0
   * @returns {this} chainable
   */
  width(width) {
    assert(width > 0, 'The width MUST be greater than 0.');
    this.options.clip.width = width;
    return this;
  }

  /**
   * Sets the height of the clipping path
   * - default is 768px @see constructor
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .width(100)
   *    .height(100) // in pixels
   * ```
   * @param {Number} height screenshot height
   * @see withDimensions(), width(), clip()
   * @throws {AssertionError} if height <= 0
   * @returns {this} chainable
   */
  height(height) {
    assert(height > 0, 'The height MUST be greater than 0.');
    this.options.clip.height = height;
    return this;
  }

  /**
   * Sets the left and top (or x and y) position of the clipping path
   * - shortcut for `.left(..).top(..)`
   * - default is 0 to 0 @see constructor
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .withDimensions(100, 100)
   *    .fromPosition(0,0)  // Take screenshot of React(0, 0, 100, 100)
   * ```
   * @param {Number} left left position  of the clipping path
   * @param {Number} top top position  of the clipping path
   * @see left(), top(), clip()
   * @returns {this} chainable
   */
  fromPosition(left, top) {
    return this.left(left).top(top);
  }

  /**
   * Sets the left or x positition of the clipping path
   * - default is 0 @see constructor
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .withDimensions(100, 100)
   *     // would take a screenshot from 100px left to 200px left
   *    .left(100)  // left start position of screenshot area / clipping path
   * ```
   * @param {Number} x left | x position  of the clipping path
   * @see fromPosition(), top(), clip()
   * @throws {AssertionError} if x is negative
   * @returns {this} chainable
   */
  left(x) {
    assert(x >= 0, 'The x coordinate MUST be greater than or equal 0.');
    this.options.clip.x = x;
    return this;
  }

  /**
   * Sets the top postition or y position of the clipping path
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .withDimensions(100, 100)
   *     // would take a screenshot from 100px left to 200px left
   *    .left(100)  // left start position of screenshot area / clipping path
   *     // would take a screenshot from 250px top to 350px top
   *    .top(250)  // top start position of screenshot area / clipping path
   * ```
   * @param {Number} y top | y position  of the clipping path
   * @see fromPosition(), left(), clip()
   * @throws {AssertionError} if y is negative
   * @returns {this} chainable
   */
  top(y) {
    assert(y >= 0, 'The y coordinate MUST be greater than or equal 0.');
    this.options.clip.y = y;
    return this;
  }

  /**
   * Sets the clipping path
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    // named clipping path
   *    .clip({x: 100, y: 100, width: 100, height: 100})
   *    // clipping path as array [left, top, width, height]
   *    .clip([100, 100, 100, 100])
   * ```
   * @param {Object|Array} clipping e.g. {[x||left]:0, [y||top]:0, width: 1024, height: 768} or as array [0, 0, 1024, 768]
   * @see fromPosition(), left(), top(), width(), height()
   * @returns {this} chainable
   */
  clip(clipping) {
    const isObject = clipping instanceof Object && !Array.isArray(clipping);
    const isArray = Array.isArray(clipping) && Array.isArray(clipping);
    /* eslint-disable no-warning-comments */
    if (isObject) { // TODO implement isReact || isObject
      return this.left(clipping.left || clipping.x)
        .top(clipping.top || clipping.y)
        .width(clipping.width)
        .height(clipping.height);
    }
    if (isArray) {
      return this.left(clipping[0])
        .top(clipping[1])
        .width(clipping[2])
        .height(clipping[3]);
    }
  }

  /**
   * Alias for `fullpage()` (ignores a previously set clipping!)
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .theWholePage() // e.g. Screenshot of 768px width and 5500px height
   * ```
   * @see fullpage(), clip()
   * @returns {this} chainable
   */
  theWholePage() {
    return this.fullpage();
  }

  /**
   * Sets a option to screencapture the whole webpage (ignores a previously set clipping!)
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .fullpage() // e.g. Screenshot of 768px width and 5500px height
   * ```
   * @see clip()
   * @returns {this} chainable
   */
  fullpage() {
    this.options.fullPage = true;

    return this;
  }

  /**
   * Sets the output format to JPEG - shortcut for `as('JPG')`
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .asJpg() // take() would returns a jpeg screenshot
   * ```
   * @see as()
   * @returns {this} chainable
   * TODO add quality parameter
   */
  asJpg() {
    return this.as('jpeg');
  }

  /**
   * Sets the output format to JPEG - shortcut for `as('JPEG')`
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .asJpeg() // take() would returns a jpeg screenshot
   * ```
   * @see as()
   * @returns {this} chainable
   * TODO add quality parameter
   */
  asJpeg() {
    return this.asJpg();
  }

  /**
   * Sets the quality of the image
   * - has no effect if png is selected as output format
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .asJpg()
   *    .withQuality(50)
   * ```
   * @param {Number} quality jpeg quality; must be an integer between 0 and 100
   * @throws {AssertionError} if options.type is not JPEG
   * @throws {AssertionError} if quality is not between 0 and 100
   * @returns {this} chainable
   */
  withQuality(quality) {
    assert(this.options.type === 'jpeg', 'Type MUST be jpeg!');
    assert(quality >= 0, 'Type MUST be greater than or equal to 0!');
    assert(quality <= 100, 'Type MUST be less than or equal to 100!');
    this.options.quality = quality;

    return this;
  }

  /**
   * Sets the output format to PNG - shortcut for `as('PNG')`
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .asPng() // take() would returns a png screenshot
   * ```
   * @see as()
   * @returns {this} chainable
   */
  asPng() {
    return this.as('png');
  }

  /**
   * Sets the output encoding
   *    'binary' - Image would be returned as Binary Stream
   *    'base64' - Image would be returned as Base64 String
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .asPng()
   *    .encode('base64') // take() would returns a base64 screenshot
   *    // or
   *    .encode('binary') // take() would returns a Buffer screenshot
   * ```
   * @param {'binary'|'base64'} encoding Screenshot encoding 'base64' or 'binary'
   * @throws {AssertionError} if encoding is not one of 'binary' or 'base64'
   * @returns {this} chainable
   */
  encode(encoding) {
    assert(['binary', 'base64'].indexOf(encoding.toLowerCase()) > -1, `The encoding MUST be one of 'binary' or 'base64'.`);
    this.options.encoding = encoding.toLowerCase();
    return this;
  }

  /**
   * Sets the output encoding to 'binary'
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .asBinary() // take() would returns a Buffer screenshot
   * ```
   * @see encode()
   * @returns {this} chainable
   */
  asBinary() {
    return this.encode('binary');
  }

  /**
   * Sets the output encoding to 'base64'
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .asBase64String() // take() would returns a base64 screenshot
   * ```
   * @see encode()
   * @returns {this} chainable
   */
  asBase64String() {
    return this.encode('base64');
  }

  /**
   * Sets the output encoding to 'base64' and saves string as file
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .asBase64File() // take() would returns a base64 screenshot / saves screenshot as textfile
   * ```
   * @param {String} filepath where the text file is to be saved
   * @see encode(), andSaveAsText()
   * @returns {this} chainable
   */
  asBase64File(filepath) {
    return this.to(filepath)
      .encode('base64')
      .andSaveAsText();
  }

  /**
   * Sets the option to save image as text file
   * @see encode(), buildPath()
   * @returns {this} chainable
   */
  andSaveAsText() {
    this.saveAsText = true;
    return this;
  }

  /**
   * Sets the output formats and output encodings
   * @example
   * ```
   *  new ScreenCapture()
   *    .of('http://example.com')
   *    .to('.')
   *    .as('binary') // take() would returns a Buffer screenshot / saves screenshot as imagefile
   *    .as('base64') // take() would returns a base64 screenshot
   *    .as('jpg') // take() saves screenshot as jpeg file
   *    .as('jpeg') // take() saves screenshot as jpeg file
   *    .as('png') // take() saves screenshot as png file
   * ```
   * @param {String} type one of 'binary', 'base64', 'jpg', 'jpeg' or 'png'
   * @see encode(), asJpg(), asJpeg(), asPng(), asBinary(), asBase64File(), asBase64String()
   * @returns {this} chainable
   */
  as(type) {
    if (['binary', 'base64'].indexOf(type.toLowerCase()) > -1) {
      return this.encode(type);
    }
    if (type.toLowerCase() === 'jpg') {
      type = 'jpeg';
    }
    assert(['jpeg', 'png'].indexOf(type.toLowerCase()) > -1);
    this.options.type = type.toLowerCase();
    return this;
  }

  /**
   * Composes the path of the image to be saved
   * @returns {String} absolute path where the screenshot should be saved
   */
  buildPath() {
    return path.join(path.resolve(this.options.path), `${this.options.name}.${this.saveAsText ? 'txt' : this.options.type}`);
  }

  /**
   * Captures and saves the screenshot
   * ```
   * // Short example "How to screencapture http://example.com"
   * new Screenshot('http://example.com')
   *    .take()
   *    .then(( res ) => console.log(res))     // Output: { path: './865ebf8b-d141-483b-a2df-eb70d5a7d05c.png', screenshot: <Buffer 89 50 4e 47 0d 0a ...>  }
   *
   * // or
   * new Screenshot('http://example.com')
   *    .take()
   *    .then(( {path} ) => console.log(path)) // Output: './865ebf8b-d141-483b-a2df-eb70d5a7d05c.png'
   *
   * // or with async / await
   * (async () =>
   *    const result = await new Screenshot('http://example.com').take()
   *    console.log(result) // Output: { path: './865ebf8b-d141-483b-a2df-eb70d5a7d05c.png', screenshot: <Buffer 89 50 4e 47 0d 0a ...>  }
   * )()
   * ```
   * @param {String} [url='null'] optional Instead of setting it in the constructor or using the .of() / .from() method
   * @async
   * @return {Object} with the path where the screenshot could be found and the screenshot itself as Stream or String
   */
  async take(url = null) {
    if (this.options.fullPage) {
      delete this.options.clip;
    }
    this.options.path = this.buildPath();
    const _url = this.url || url;
    const browser = await puppeteer.launch(this.options.launch);
    let screenshot = null;
    try {
      const page = await browser.newPage();
      await page.goto(_url);
      screenshot = await page.screenshot(this.options);
    } catch (err) {
      await browser.close();
      throw err;
    }
    await browser.close();
    return {path: this.options.path, screenshot};
  }
}

module.exports = {ScreenCapture};
