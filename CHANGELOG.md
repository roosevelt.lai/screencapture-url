# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.0.0] - 2018-08-12
### Added
- API.md with jsdoc2md generated
- sourcecode API documentation
- ivyowl's alpine-node-pupetteer docker image
- gitlab-ci
- TAP Tests and coverage
- CHANGELOG, LICENSE, README basics
- Basic functionality and Basic Structure
