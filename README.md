# screencapture-url
Capture a screenshot of a webpage using Google's puppeteer

[![Build Status](https://gitlab.com/amlang/screencapture-url/badges/master/build.svg)](https://gitlab.com/amlang/screencapture-url/pipelines)  [![Code Coverage](https://gitlab.com/amlang/screencapture-url/badges/master/coverage.svg)](https://gitlab.com/amlang/screencapture-url/pipelines) [![MIT License](https://img.shields.io/badge/licence-MIT-blue.svg)](https://opensource.org/licenses/MIT) [![NPM screencapture-url](https://img.shields.io/npm/v/screencapture-url.svg)](https://npmjs.org/package/screencapture-url) [![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/xojs/xo) [![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgitlab.com%2Famlang%2Fscreencapture-url.svg?type=shield)](https://app.fossa.io/projects/git%2Bgitlab.com%2Famlang%2Fscreencapture-url?ref=badge_shield)


## Installation
```bash
$ yarn add screencapture-url
```
or
```bash
$ npm install screencapture-url --save
```
## Usage
Full API documentation see [API.md](./API.md)
```
// fast example
(async () =>
   const result = await new Screenshot()
       .of('http://example.com')
       .to('.')
       .withName('my_screenshot')
       .asPng()
       .take()
   console.log(result) // Output: { path: './my_screenshot.png', screenshot: <Buffer 89 50 4e 47 0d 0a ...>  }
)()
```

## See also
[GoogleChrome/puppeteer](https://github.com/GoogleChrome/puppeteer)

## License
MIT, see [LICENSE.md](./LICENSE.md) for more details.

[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgitlab.com%2Famlang%2Fscreencapture-url.svg?type=large)](https://app.fossa.io/projects/git%2Bgitlab.com%2Famlang%2Fscreencapture-url?ref=badge_large)